<?php

date_default_timezone_set('Europe/Kiev');

require_once __DIR__ . '/../../vendor/autoload.php';
$appConf = include_once __DIR__ . '/../../etc/app-conf.php';
$propel = include_once __DIR__ . '/../../etc/propel.php';

Engine::create($propel['propel']['database']['connections']['engine']);

$app = AppFactory::create($appConf);
$app->run();
