<?php

$tplDir = dirname(__FILE__).'/../templates';
$tmpDir = dirname(__FILE__).'/../cache/trans';

require_once(dirname(__FILE__).'/../vendor/autoload.php');

//Twig_Autoloader::register();

$loader = new Twig_Loader_Filesystem($tplDir);

// force auto-reload to always have the latest version of the template
$twig = new Twig_Environment($loader, array(
    'cache' => $tmpDir,
    'auto_reload' => true
));

$twig->addExtension(new Twig_Extensions_Extension_I18n());

// configure Twig the way you want
$function1 = new \Twig_SimpleFunction('path_for', array($twig, 'pathFor'));
$twig->addFunction($function1);
$function2 = new \Twig_SimpleFunction('base_url', array($twig, 'baseUrl'));
$twig->addFunction($function2);

// iterate over all your templates
foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($tplDir), RecursiveIteratorIterator::LEAVES_ONLY) as $file)
{
    // force compilation
    if ($file->isFile()) {
        $twig->loadTemplate(str_replace($tplDir.'/', '', $file));
    }
}

$list = ['ru_RU', 'en_US'];
foreach ($list as $locale) {
    exec("xgettext --default-domain=slim -p ./../locales/client/$locale/LC_MESSAGES/ --from-code=utf-8 -n -j -L PHP ./../cache/trans/*/*.php");
    $pathToPo = dirname(__DIR__) . "/locales/client/$locale/LC_MESSAGES/slim.po";

    $contents = file_get_contents($pathToPo);
    $contents = preg_replace('/^#(,|:).*$/im', '', $contents);
    $contents = preg_replace('/\n{3,}/im', PHP_EOL . PHP_EOL, $contents);

    file_put_contents($pathToPo, $contents);
}

