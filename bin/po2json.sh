#!/usr/bin/env bash

pushd `dirname $0` > /dev/null;
ROOT_DIR=`pwd`'/..';
popd > /dev/null;

node "${ROOT_DIR}/node_modules/po2json/bin/po2json" "${ROOT_DIR}/locales/admin/en.po" -f jed1.x -p "${ROOT_DIR}/public/admin/static/locales/admin/en.json";
node "${ROOT_DIR}/node_modules/po2json/bin/po2json" "${ROOT_DIR}/locales/admin/ru.po" -f jed1.x -p "${ROOT_DIR}/public/admin/static/locales/admin/ru.json";
node "${ROOT_DIR}/node_modules/po2json/bin/po2json" "${ROOT_DIR}/locales/admin/ua.po" -f jed1.x -p "${ROOT_DIR}/public/admin/static/locales/admin/ua.json";
node "${ROOT_DIR}/node_modules/po2json/bin/po2json" "${ROOT_DIR}/locales/admin/en_seo.po" -f jed1.x -p "${ROOT_DIR}/public/admin/static/locales/admin/en_seo.json";
node "${ROOT_DIR}/node_modules/po2json/bin/po2json" "${ROOT_DIR}/locales/admin/ru_seo.po" -f jed1.x -p "${ROOT_DIR}/public/admin/static/locales/admin/ru_seo.json";
