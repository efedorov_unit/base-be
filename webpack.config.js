var webpack = require("webpack");
var path = require("path");

var nodeDir = __dirname + "/node_modules";
var adminLte = nodeDir + "/admin-lte";

var ExtractTextPlugin = require("extract-text-webpack-plugin");

var autoprefixer = require('autoprefixer');

module.exports = {
    resolve: {
        alias: {
            jqueryUi: adminLte + "/plugins/jQueryUI/jquery-ui.min.js",
            dataTableBsCss: nodeDir + '/datatables.net-bs/css/dataTables.bootstrap.css',
            admBootstrap: adminLte + "/bootstrap/js/bootstrap.min.js",
            bootstrapCss: nodeDir + "/bootstrap/dist/css/bootstrap.min.css",
            admCss: adminLte + "/dist/css/AdminLTE.min.css",
            admSkin: adminLte + "/dist/css/skins/_all-skins.min.css",
            trumbowygJs: nodeDir + "/trumbowyg/dist/trumbowyg.min.js",
            trumbowygCss: nodeDir + "/trumbowyg/dist/ui/trumbowyg.min.css"
        }
    },
    entry: [
        "./client/src/index.js"
    ],
    output: {
        path: __dirname + "/public/admin/build/",
        filename: "app.js",
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({output: {comments: false}}),
        new webpack.NoErrorsPlugin(),
        new webpack.optimize.OccurenceOrderPlugin(true),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery"
        }),
        new ExtractTextPlugin("[name].css")
    ],
    module: {
        loaders: [
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract("style-loader", "css-loader!postcss-loader")
            },
            {
                test: /\.less$/,
                loader: ExtractTextPlugin.extract("style-loader", "css-loader!postcss-loader!less-loader")
            },

            { test: /\.gif$/, loader: "url-loader?limit=10000&mimetype=image/gif" },
            { test: /\.jpg$/, loader: "url-loader?limit=10000&mimetype=image/jpg" },
            { test: /\.png$/, loader: "url-loader?limit=10000&mimetype=image/png" },
            { test: /\.svg$/, loader: "url-loader?limit=26000&mimetype=image/svg+xml" },
            { test: /\.(eot|svg|ttf|woff2?)(\?\w+)?$/i, loader: "url-loader" },

            { test: /\.js$/, loader: "babel-loader", exclude: [/node_modules/] },

            { test: /\.json$/, loader: "json-loader" }
        ],
        resolve: {
          extensions: ["", ".js"]
        }
    },
    postcss: function() {
        return [autoprefixer];
    },
    eslint: {
        configFile: ".eslintrc"
    },
    devServer: {
        headers: { "Access-Control-Allow-Origin": "*" }
    },
    noParse: /\/node_modules\/jquery\//
};
