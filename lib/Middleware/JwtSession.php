<?php

namespace Middleware;

use Psr\Http\Message\RequestInterface as ReqInt;
use Psr\Http\Message\ResponseInterface as ResInt;
use Firebase\JWT\JWT;

final class JwtSession
{
    private $options = [
        'secret'    => '',
        'algorithm' => ['HS512'],
        'attribute' => 'session',
        'header'    => 'HTTP_AUTHORIZATION',
    ];

    public function __construct($options = [])
    {
        $this->options = $options + $this->options;
    }

    public function __invoke(ReqInt $req, ResInt $res, callable $next)
    {
        try {
            $headers = $req->getHeader($this->options['header']);
            $token = isset($headers[0]) ? $headers[0] : "";

            if ($token) {
                $decoded = JWT::decode(
                    $token,
                    $this->options['secret'],
                    (array) $this->options['algorithm']
                );
                $req = $req->withAttribute($this->options["attribute"], $decoded);
            }
        } catch (\Exception $exception) {
            // error_log($exception->getMessage());
            // $this->log(LogLevel::WARNING, $exception->getMessage(), [$token]);
            // return false;
        }

        return $next($req, $res);
    }
}
