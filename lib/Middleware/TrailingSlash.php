<?php

namespace Middleware;

use Psr\Http\Message\RequestInterface as ReqInt;
use Psr\Http\Message\ResponseInterface as ResInt;

final class TrailingSlash
{
    private $addSlash;

    public function __construct($addSlash = true)
    {
        $this->addSlash = (bool)$addSlash;
    }

    public function __invoke(ReqInt $req, ResInt $res, callable $next)
    {
        $uri = $req->getUri();
        $path = $uri->getPath();

        if ($this->addSlash) {
            if (strlen($path) > 1 && substr($path, -1) !== '/' && !pathinfo($path, PATHINFO_EXTENSION)) {
                $path .= '/';
            }
        } else {
            if (strlen($path) > 1 && substr($path, -1) === '/') {
                $path = substr($path, 0, -1);
            }
        }

        if ($path === '') {
            $path = '/';
        }

        return $next($req->withUri($uri->withPath($path)), $res);
    }
}
