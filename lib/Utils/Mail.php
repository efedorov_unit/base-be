<?php

namespace Utils;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;

final class Mail
{
    public static function send($mail, $config, $log)
    {
        $mailConf = $config['email'];

        switch ($mailConf['transport']) {
            case 'none':
                return true;
            case 'smtp':
                $transport = (new \Swift_SmtpTransport($mailConf['server'], $mailConf['port']))
                    ->setUsername($mailConf['username'])
                    ->setPassword($mailConf['password'])
                    ->setEncryption($mailConf['encryption']);
                break;
            default:
                $transport = \Swift_MailTransport::newInstance();
                break;
        }

        $message = (new \Swift_Message($mail['subject']))
            ->setFrom(!empty($mail['from']) ? $mail['from'] : $mailConf['username'])
            ->setTo($config['feedback_email'])
            ->setBody($mail['body'], 'text/html');

        foreach ($mail['files'] as $file) {
            if ($file->file) {
                $attachment = \Swift_Attachment::fromPath($file->file)->setFilename($file->getClientFilename());
                $message->attach($attachment);
            }
        }

        $res = (new \Swift_Mailer($transport))->send($message, $failures);

        foreach ($failures as $failure) {
            $log->debug('Failed message sending to ' . $failure);
        }

        return $res;
    }
}
