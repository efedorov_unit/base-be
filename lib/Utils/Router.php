<?php

namespace Utils;

class Router
{
    private $route;
    private $config;
    private $lang;

    /**
     * Create object
     */
    public function __construct(\Slim\Router $router, $config, $lang)
    {
        $this->router = $router;
        $this->config = $config;
        $this->lang = $lang;

        return $this;
    }

    /**
     *  Redefined pathFor method
     */
    public function pathFor($name, array $data = [], array $queryParams = [])
    {
        $url = $this->router->pathFor($name, $data, $queryParams);
        $langPrefix = ($this->lang !== 'en') ? ('/' . $this->lang) : '';
        $url = $langPrefix . $url;

        return $url;
    }

    /**
     *  Auto redefined Slim\Router methods
     *      @see        Magic methods in PHP
     *      @param      string      $method     - method name
     *      @param      array       $arguments  - arguments list
     *      @return     mix                     - result of calling \Slim\Router '$method'
     */
    public function __call($method, $arguments)
    {
        $router = $this->router;

        if (method_exists($router, $method)) {
            return call_user_func_array(array(&$router, $method), $arguments);
        }
    }
}
