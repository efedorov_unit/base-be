<?php

namespace Script;

use Propel\Runtime\Propel;
use \Monolog\Logger as Logger;
use \Monolog\Handler\StreamHandler as StreamHandler;
use \Monolog\Handler\ErrorLogHandler as ErrorLogHandler;
use Service\X;

const PID_DIR = '/tmp';

abstract class Base
{
    protected $name   = '';
    protected $log    = null;
    protected $config = [];

    public function __construct($name, $config)
    {
        $this->config = $config;
        $this->name = $name;

        $this->log = new Logger($name);
        $this->log->pushHandler(new StreamHandler(__DIR__."/../../logs/$name.log", Logger::DEBUG));
        $this->log->pushHandler(new ErrorLogHandler());

        if ($config['debugMode']) {
            Propel::getServiceContainer()->setLogger('defaultLogger', $this->log);
            $con = Propel::getWriteConnection(\Engine\Map\UserTableMap::DATABASE_NAME);
            $con->useDebug(true);
        }
    }

    final public function run()
    {
        $this->start();

        try {
            $this->main();
        } catch (X $e) {
            $error = json_encode($e->getError());
            $this->log->error(
                "ERROR: Caught Exception in script {$this->name} with error: {$error}"
            );
        } catch (\Exception $e) {
            $this->log->error(
                "ERROR: Caught Exception in script {$this->name} with message: {$e->getMessage()}"
            );
        }

        $this->finish();
    }

    /**
     * Start script create PID file
     *
     * @return void nothing to return
     */
    private function start()
    {
        if (function_exists('posix_kill')) {
            $this->log->info("START {$this->name} script");

            $pidFilePath = PID_DIR."/{$this->name}.pid";

            if (file_exists($pidFilePath)) {
                $pid = file_get_contents($pidFilePath);

                if (posix_kill((int)$pid, 0)) {
                    $this->log->info('Script already running. PID = ' . $pid);
                    exit();
                }

                $this->log->info('WARN: pid file exists, but process stoped: ' . $pid);
            }

            file_put_contents($pidFilePath, posix_getpid());
        }
    }

    /**
     * Finish script and remove PID file
     *
     * @return void nothing to return
     */
    private function finish()
    {
        $this->log->info("FINISH {$this->name} script");

        unlink(PID_DIR."/{$this->name}.pid");
    }

    protected function action($class)
    {
        return new $class([
            'log'    => $this->log,
            'config' => $this->config
        ]);
    }

    /**
     * Entry point for script
     *
     * @return void Nothing to return for cron script
     */
    abstract protected function main();
}
