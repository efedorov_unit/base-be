<?php

namespace Controller\System;

use Controller\Base;
use Psr\Http\Message\RequestInterface as ReqInt;
use Psr\Http\Message\ResponseInterface as ResInt;

class UserRole extends Base
{
    public function index(ReqInt $req, ResInt $res, array $args) : ResInt
    {
        $self = $this;
        $data = $req->getQueryParams();

        return $this->run(function () use ($self, $data) {
            return $self->action('\Service\System\UserRole\Index')->run($data);
        }, $res);
    }
}
