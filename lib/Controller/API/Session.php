<?php

namespace Controller\API;

use Psr\Http\Message\RequestInterface as ReqInt;
use Psr\Http\Message\ResponseInterface as ResInt;

class Session extends \Controller\Base
{
    public function create(ReqInt $req, ResInt $res, array $args) : ResInt
    {
        $self = $this;
        $data = $req->getParsedBody();

        return $this->run(function () use ($self, $data) {
            return $self->action('\Service\SessionAPI\Create')->run($data);
        }, $res);
    }
}
