<?php

namespace Controller\Admin;

use Psr\Http\Message\ServerRequestInterface as ReqInt;
use Psr\Http\Message\ResponseInterface as ResInt;

class Home extends \Controller\Base
{
    public function show(ReqInt $req, ResInt $res, array $args) : ResInt
    {
        return $this->render(
            $res,
            'admin/index.html.twig',
            []
        );
    }
}
