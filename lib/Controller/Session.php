<?php

namespace Controller;

use Psr\Http\Message\RequestInterface as ReqInt;
use Psr\Http\Message\ResponseInterface as ResInt;

class Session extends Base
{
    public function create(ReqInt $req, ResInt $res, array $args) : ResInt
    {
        $self = $this;
        $data = $req->getParsedBody();

        return $this->run(function () use ($self, $data) {

            $result = $self->action('\Service\Session\Create')->run($data);

            if ($result && isset($result['User'])) {
                \Utils\Session::set('UserId', $result['User']['Id']);
                return ['Status' => 1];
            }
        }, $res);
    }
}
