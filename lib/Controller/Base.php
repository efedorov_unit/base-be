<?php

namespace Controller;

use Psr\Http\Message\ServerRequestInterface as ReqInt;
use Psr\Http\Message\ResponseInterface as ResInt;
use Utils\ReCaptcha;

abstract class Base
{
    protected $container;

    /**
     * Create object
     */
    public function __construct(\Slim\Container $container)
    {
        $this->container = $container;
        $this->lang(); // init lang

        return $this;
    }

    /**
     *  Object log() - return log property for Slim object. It was defined in RestAPI script
     */
    public function log()
    {
        return $this->container['log'];
    }

    /**
     *  Object config() - return config
     */
    public function config()
    {
        return $this->container['config'];
    }


    /**
     *  String lang() - return current lang
     */
    public function lang()
    {
        return $this->container['lang'];
    }

    /**
     * ResInt run( function $cb, ResInt $response ) - action wrapper
     */
    public function run(callable $cb, ResInt $response)
    {
        try {
            $result = call_user_func($cb);
        } catch (\Service\X $e) {
            $result = $e->getError();

            $this->log()->debug('Service Exception: ' . ($result['Error']['Message'] ?: $result['Error']['Type']));
        }

        return $response->withJson($result);
    }

    /**
     * Object action( string $class ) - create service object
     */
    public function action($class, $session = null)
    {
        return new $class([
            'log'    => $this->log(),
            'lang'   => $this->lang(),
            'config' => $this->config(),
            'UserId' => isset($session->id) ? $session->id : \Utils\Session::get('UserId'),
        ]);
    }

    public function notFound(ReqInt $req, ResInt $res, array $args = [])
    {
        return $this->container['notFoundHandler']($req, $res, $args);
    }

    public function mail()
    {
        return $this->container['mail'];
    }

    /**
     *  Object view() - return view object
     */
    public function view()
    {
        return $this->container['view'];
    }

    /**
     *  Object env() - return environment object
     */
    public function env()
    {
        return $this->container['environment'];
    }

    /**
     *  void render(ResInt $res, string $temaplte)  - template render method
     *      @param      ResInt      $res        - responce object
     *      @param      string      $template   - template file
     *      @param      array       $args       - arguments
     *      @return     void
     */
    public function render(ResInt $res, $template, $args = [])
    {
        $args += $this->config()['application'];

        $this->view()->render($res, $template, $args);
        return $res;
    }

    public function renderMail($template, $args = [])
    {
        $args += $this->config()['application'];
        $res = $this->mail()->fetch($template, $args);
        return $res;
    }

    public function sendMail($params)
    {
        $body = $this->renderMail($params['Template'], $params['TemplateArgs']);

        return \Utils\Mail::send(
            [
                'to'      => $params['To'],
                'subject' => $params['Subject'],
                'body'    => $body
            ],
            $this->config(),
            $this->log()
        );
    }

    /** Merge Request data with Form Data
     *
     * @param $data array Request data
     * @return mixed
     */
    public function mergeWithFormData($data)
    {
        if (isset($data['data']) && is_string($data['data'])) {
            $data = json_decode($data['data'], true);
        }

        return $data;
    }

    /** Merge Data with  Request Files array
     * @param $data array
     * @return array
     */
    public function mergeWithFiles($data)
    {
        return array_merge($data, $_FILES);
    }
}
