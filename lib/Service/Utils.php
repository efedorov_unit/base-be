<?php

namespace Service;

use Engine\Map\UserTableMap;
use Propel\Runtime\Propel;
use Symfony\Component\Validator\Constraints\DateTime;

trait Utils
{
    public function defaultParams(array $params, $field)
    {
        $defaults = [
            'Search'    => '',
            'Limit'     => 0,
            'Offset'    => 0,
            'SortField' => $field,
            'SortOrder' => 'asc',
        ];

        return array_merge($defaults, $params);
    }

    /**
     *  String getExtension($base) - return image extension from base64 img
     *      @param       string      $base      - base64 image string
     *      @return      string
     */
    public function getExtension($base)
    {
        $f = finfo_open();
        $mimeType = finfo_buffer($f, $base, FILEINFO_MIME_TYPE);
        finfo_close($f);

        return str_replace('image/', '', $mimeType);
    }

    /**
     * @param $function
     *
     * @return mixed
     * @throws \Exception
     */
    public function transaction($function)
    {
        $con = Propel::getWriteConnection(UserTableMap::DATABASE_NAME);

        return $con->transaction($function);
    }

    /**
     * Array toKeyIndex(array $arr, $keyColumn) - get an associative array.
     * The second parameter specifies the column to be used for the key.
     *
     * @param array  $arr
     * @param string $keyColumn
     *
     * @return array
     * @internal param $keyColumn
     *
     */
    public function toKeyIndex(array $arr, $keyColumn)
    {
        $result = [];
        foreach ($arr as $item) {
            $result[$item[$keyColumn]] = $item;
        }

        return $result;
    }
}
