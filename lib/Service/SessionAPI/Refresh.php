<?php

namespace Service\SessionAPI;

use Service\X;
use Service\Base;
use Service\Validator;

final class Refresh extends Base
{
    use Jwt;

    final protected function validate(array $params)
    {
        return [];
    }

    final protected function execute(array $params)
    {
        $user = $this->getUser();
        $jwt = $this->createUserJwt($user);

        return [
            'Status'            => 1,
            'JWT'               => $jwt,
            'UserRelatedData'   => $user->getUserRelatedData()
        ];
    }
}
