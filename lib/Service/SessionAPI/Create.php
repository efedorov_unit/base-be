<?php

namespace Service\SessionAPI;

use Service\Base;

final class Create extends Base
{
    final protected function validate(array $params)
    {
        return $params;
    }

    final protected function execute(array $params)
    {
        return [
            'Status'            => 1
        ];
    }
}
