<?php

namespace Service\X;

class AccessDenied extends \Service\X
{
    protected $type = 'ACCESS_DENIED';

    public function getError()
    {
        return [
            'Error' => [
                'Type' => $this->type,
            ]
        ];
    }
}
