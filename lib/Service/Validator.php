<?php

namespace Service;

class Validator
{
    public static function validate($data, $livr)
    {
        \Validator\LIVR::registerDefaultRules([
            'coin_year'  => function ($field) {
                return function ($value, $params) use ($field) {
                    if (!isset($value) || $value === '') {
                        return;
                    }

                    if (!\Validator\LIVR\Util::isStringOrNumber($value)) {
                        return 'FORMAT_ERROR';
                    }

                    if ($value < 100 || $value > date('Y')) {
                        return 'INVALID_YEAR';
                    }

                    return;
                };
            },

            'greater_than_field'  => function ($field) {
                return function ($value, $params) use ($field) {
                    if (!isset($value) || $value === '') {
                        return;
                    }

                    if (!\Validator\LIVR\Util::isStringOrNumber($value)) {
                        return 'FORMAT_ERROR';
                    }

                    if ($value > $params[$field]) {
                        return;
                    }

                    return 'FIELD_IS_NOT_GREATER';
                };
            },

            'lesser_than_field'  => function ($field) {
                return function ($value, $params) use ($field) {
                    if (!isset($value) || $value === '') {
                        return;
                    }

                    if (!\Validator\LIVR\Util::isStringOrNumber($value)) {
                        return 'FORMAT_ERROR';
                    }

                    if ($value < $params[$field]) {
                        return;
                    }

                    return 'FIELD_IS_NOT_LESSER';
                };
            },

            'file' => function ($field) {
                return function ($value) use ($field) {
                    if (!isset($value) || $value === '') {
                        return;
                    }

                    if ($value instanceof \Psr\Http\Message\UploadedFileInterface) {
                        $error = $value->getError();

                        if ($error === UPLOAD_ERR_OK) {
                            return;
                        }

                        return 'FILE_UPLOADING_ERROR';
                    }

                    return 'NOT_A_FILE';
                };
            },

            'one_of_mime_type' => function () {
                $first_arg = func_get_arg(0);

                if (is_array($first_arg) && !\Validator\LIVR\Util::isAssocArray($first_arg)) {
                    $allowedValues = $first_arg;
                } else {
                    $allowedValues = func_get_args();
                    array_pop($allowedValues); # pop rule_builders
                }

                $modifiedAllowedValues = array();
                foreach ($allowedValues as $v) {
                    $modifiedAllowedValues[] = (string) $v;
                }

                return function ($value, $params) use ($modifiedAllowedValues) {
                    if (!isset($value) || $value === '') {
                        return;
                    }

                    if (!($value instanceof \Psr\Http\Message\UploadedFileInterface)) {
                        return;
                    }

                    $type = $value->getClientMediaType();

                    if (in_array($type, $modifiedAllowedValues)) {
                        return;
                    }

                    return 'NOT_AVALIABLE_MIME_TYPE';
                };
            },

            'not_is_one_of' => function () {
                $notAllowedValues = func_get_args();
                array_pop($notAllowedValues);

                return function ($value) use ($notAllowedValues) {
                    if (!isset($value) || $value === '') {
                        return;
                    }

                    if (!in_array($value, $notAllowedValues)) {
                        return;
                    }

                    return 'NOT_ALLOWED_VALUE';
                };
            },

            'in_array'  => function () {
                $allowedValues = func_get_args();
                array_pop($allowedValues);

                return function ($valuesArr) use ($allowedValues) {
                    if (!isset($valuesArr) || $valuesArr === '') {
                        return;
                    }

                    foreach ($valuesArr as $key => $value) {
                        if (!in_array($value, $allowedValues)) {
                            return [
                                $key => 'NOT_ALLOWED_VALUE'
                            ];
                        }
                    }

                    return;
                };
            },
        ]);

        $validator = new \Validator\LIVR($livr);

        $validated = $validator->validate($data);
        $errors    = $validator->getErrors();

        if ($errors) {
            throw new X(['Type' => 'FORMAT_ERROR', 'Fields' => $errors]);
        }

        return $validated;
    }
}
