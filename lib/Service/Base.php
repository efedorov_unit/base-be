<?php

namespace Service;

use Engine\Context;
use Engine\UserRoleMapQuery;
use Engine\UserQuery;

abstract class Base
{
    use Utils;

    private $log;
    private $config;
    private $lang;
    private $UserId;
    protected $withoutPermissionVerification;

    public function __construct($attrs)
    {
        $this->log = $attrs['log'];
        $this->config = $attrs['config'];
        $this->withoutPermissionVerification = $attrs['withoutPermissionVerification'] ?? false;


        if (isset($attrs['lang'])) {
            $this->lang = $attrs['lang'];
        }

        if (isset($attrs['UserId'])) {
            $this->UserId = $attrs['UserId'];
        }

        if ($this->lang) {
            Context::setLang($this->lang);
        }
    }

    abstract protected function validate(array $params);

    abstract protected function execute(array $params);

    protected function lang()
    {
        return $this->lang;
    }

    protected function getUserId()
    {
        return $this->UserId;
    }

    protected function getUser()
    {
        return UserQuery::create()
            ->filterByStatus('active')
            ->filterById($this->getUserId())
            ->findOne();
    }

    protected function log()
    {
        return $this->log;
    }

    protected function config()
    {
        return $this->config;
    }

    final public function run(array $params = [])
    {
        try {
            $validated = $this->validate($params);
            $result = $this->execute($validated);

            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
