<?php

namespace Service\Session;

use Engine\Context;
use Engine\UserQuery;
use Engine\UserRoleMapQuery;
use Service\X;
use Service\Validator;
use Service\Base;

final class Create extends Base
{
    final protected function validate(array $params)
    {
        $rules = [
            'Email'       => [ 'required', 'email', 'to_lc' ],
            'Password'    => [ 'required' ]
        ];

        return Validator::validate($params, $rules);
    }

    final protected function execute(array $params)
    {
        return [
            'Status' => 1
        ];
    }
}
