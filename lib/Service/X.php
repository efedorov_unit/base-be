<?php

namespace Service;

class X extends \Exception
{
    protected $fields;
    protected $type;
    protected $myMessage = '';

    public function __construct($attrs = [])
    {
        $this->myMessage = isset($attrs['Message']) ? $attrs['Message'] : '';
        $this->fields = isset($attrs['Fields']) ? $attrs['Fields'] : null;
        $this->type = isset($attrs['Type']) ? $attrs['Type'] : null;
    }

    public function getError()
    {
        return [
            'Status' => 0,
            'Error'  => [
                'Fields'  => $this->fields,
                'Type'    => $this->type,
                'Message' => $this->myMessage,
            ]
        ];
    }
}
