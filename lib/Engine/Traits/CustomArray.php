<?php
namespace Engine\Traits;

use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;

trait CustomArray
{
    public function format($fields)
    {
        $extendedArray = [];

        foreach ($fields as $key => $field) {
            if (is_string($key) && !is_array($field)) {
                $getter = "get{$key}";

                $value = $this->$getter($field);

                if ($value instanceof Collection) {
                    $arrayOfExtendedObjects = [];
                    foreach ($value as $item) {
                        $arrayOfExtendedObjects[] = $item->toArray();
                    }
                    $extendedArray[$key] = $arrayOfExtendedObjects;
                } elseif ($value instanceof ActiveRecordInterface) {
                    $extendedArray[$key] = $value->toArray();
                } else {
                    $extendedArray[$key] = $value;
                }
            } elseif (is_string($key) && is_array($field)) {
                $getter = "get{$key}";
                // if (method_exists($this, $getter)) {
                //     continue;
                // }
                $extendedArray[$key] = $this->$getter();

                if (!is_null($extendedArray[$key])) {
                    if ($extendedArray[$key] instanceof Collection) {
                        $arrayOfExtendedObjects = [];
                        foreach ($extendedArray[$key] as $item) {
                            $arrayOfExtendedObjects[] = $item->format($field);
                        }
                        $extendedArray[$key] = $arrayOfExtendedObjects;
                    } elseif ($extendedArray[$key] instanceof ActiveRecordInterface) {
                        $extendedArray[$key] = $extendedArray[$key]->format($field);
                    }
                }
            } else {
                $getter = "get{$field}";
                // if (!method_exists($this, $getter)) {
                //     return $extendedArray;
                // }
                $extendedArray[$field] = $this->$getter();

                if ($extendedArray[$field] instanceof \DateTime) {
                    $extendedArray[$field] = $extendedArray[$field]->getTimestamp();
                } elseif ($extendedArray[$field] instanceof ActiveRecordInterface) {
                    $extendedArray[$field] = $extendedArray[$field]->toArray();
                } elseif ($extendedArray[$field] instanceof Collection) {
                    $arrayOfExtendedObjects = [];
                    foreach ($extendedArray[$field] as $item) {
                        $arrayOfExtendedObjects[] = $item->toArray();
                    }
                    $extendedArray[$field] = $arrayOfExtendedObjects;
                }
            }
        }

        return $extendedArray;
    }

    public static function formatCollection($items, $fields)
    {
        $data = [];

        foreach ($items as $item) {
            $data[] = $item->format($fields);
        }

        return $data;
    }
}
