<?php

namespace Engine\Traits;

trait PutEnvTrait
{
    public static function setLocale($locales = [], $lang = 'en')
    {
        $locale = $locales[$lang] ?? 'en_US';

        putenv("LC_ALL=$locale");
        putenv("LANGUAGE=$locale");
        setlocale(LC_COLLATE, $locale);
        setlocale(LC_ALL, $locale);

        bindtextdomain('slim', __DIR__ . '/../../../locales/client/');
        bind_textdomain_codeset('slim', 'UTF-8');
        textdomain('slim');
    }
}
