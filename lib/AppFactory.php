<?php

namespace

{
    use Monolog\Handler\StreamHandler;
    use Monolog\Logger;
    use Propel\Runtime\Propel;
    use Psr\Http\Message\RequestInterface as ReqInt;
    use Psr\Http\Message\ResponseInterface as ResInt;

    class AppFactory
    {
        public static function create($config)
        {
            // Create monolog logger and store logger in container
            $container = self::containerization($config);

            // Prepare app
            $app = new \Slim\App($container);

            if ($config['debugMode']) {
                $app->add(new \Tuupola\Middleware\Cors([
                    'headers.allow' => ['content-type', 'Authorization']
                ]));
                $con = Propel::getWriteConnection(Engine\Map\UserTableMap::DATABASE_NAME);
                $con->useDebug(true);
            }

            $logger = new Logger('propel');
            $logger->pushHandler(new StreamHandler(
                $config['monolog']['loggerOptions']['propel'],
                Logger::DEBUG
            ));

            Propel::getServiceContainer()->setLogger('defaultLogger', $logger);

            $app->add(new Middleware\SessionCookies(['lifetime' => '1 day']));
            $app->add(new Middleware\TrailingSlash);

            $isAuth   = new Middleware\IsAuth;
            $isAuthJWT = new \Slim\Middleware\JwtAuthentication([
                'secret'    => $config['jwt_key'],
                'algorithm' => ['HS512'],
                'attribute' => 'session',
                'header'    => 'HTTP_AUTHORIZATION',
                'regexp'    => '/(.*)/',
                'relaxed'   => ['localhost', '127.0.0.1', ''],
                'secure'    => false
            ]);
            $parseJwtSession = new Middleware\JwtSession([
                'secret'    => $config['jwt_key'],
                'algorithm' => ['HS512'],
                'attribute' => 'session',
                'header'    => 'HTTP_AUTHORIZATION'
            ]);

            $app->group('/admin', function () use ($container) {
                $home = new Controller\Admin\Home($container);
                $this->get('/', [$home, 'show']);
            });

            // Define API routes
            $app->group('/api/v1', function () use ($container, $isAuth) {
            });

            $app->group('/site-api/v1', function () use ($container, $isAuthJWT) {
            })->add($parseJwtSession);

            return $app;
        }

        private static function containerization($config)
        {
            $container = new \Slim\Container;

            $container['log'] = function () use ($config) {
                $log = new \Monolog\Logger($config['monolog']['name']);

                foreach ($config['monolog']['handlers'] as $option) {
                    $handler = new \Monolog\Handler\StreamHandler($option['filepath']);

                    // set format if need
                    if (isset($option['format']) && $option['format']) {
                        $handler->setFormatter(
                            new Monolog\Formatter\LineFormatter($option['format'])
                        );
                    }

                    $log->pushHandler($handler);
                }

                return $log;
            };

            $container['config'] = $config;

            $container['customRouter'] = function ($c) {
                return new \Utils\Router($c->get('router'), $c->get('config'), $c->get('lang'));
            };

            $container['view'] = function ($c) {
                $config = $c->get('config');
                $view = new Slim\Views\Twig($config['view']['template_path'], $config['view']['twig']);

                $router = $c->get('customRouter');
                // Add extensions

                $view->addExtension(new Slim\Views\TwigExtension($router, $c->get('request')->getUri()));
                $view->addExtension(new Twig_Extension_Debug());
                $view->addExtension(new Twig_Extensions_Extension_I18n());

                return $view;
            };

            $container['mail'] = function ($c) {
                $config = $c->get('config');
                $view = new Slim\Views\Twig($config['email']['template_path'], $config['email']['twig']);

                $router = $c->get('customRouter');
                // Add extensions

                $view->addExtension(new Slim\Views\TwigExtension($router, $c->get('request')->getUri()));
                $view->addExtension(new Twig_Extension_Debug());
                $view->addExtension(new Twig_Extensions_Extension_I18n());

                return $view;
            };

            $container['notFoundHandler'] = function ($с) {
                return function ($request, $response) use ($с) {
                    $controller = new Controller\NotFound($с);
                    $response = $controller->show($request, $с['response'], []);
                    return $с['response']
                        ->withStatus(404);
                };
            };

            /* TODO change default language */
            $container['lang'] = function ($c) {
                $path = $c->get('request')->getUri()->getPath();
                $locales = $c->get('config')['locales'] ?? [];

                $defaultLang = 'en';

                /* set default locale for translation */
                $defaultLocale = $locales[$defaultLang] ?? 'en';

                putenv('LC_ALL=' . $defaultLocale);
                putenv('LANGUAGE=' . $defaultLocale);
                setlocale(LC_ALL, $defaultLocale);

                $l = $defaultLang;

                foreach ($locales as $lang => $locale) {
                    if (preg_match("/^\/$lang/", $path)) {
                        putenv("LC_ALL=$locale");
                        putenv("LANGUAGE=$locale");
                        setlocale(LC_COLLATE, $locale);
                        setlocale(LC_ALL, $locale);
                        $l = $lang;

                        break;
                    }
                }

                bindtextdomain('slim', __DIR__ . '/../locales/client/');
                bind_textdomain_codeset('slim', 'UTF-8');
                textdomain('slim');

                return $l;
            };

            return $container;
        }
    }
}
