import cookie from 'cookie';
import locale from './Modules/locale.js';

export default class Main {
    async init() {
        const localeData = await this.initLocale();
        locale.init({ localeData, locale: window.lang });
    }

    initLocale() {
        const cookieLang = cookie.parse(document.cookie).locale;

        window.lang = cookieLang && cookieLang !== 'null' ? cookieLang : 'ru';

        return fetch(`/admin/static/locales/admin/${  window.lang  }.json`, {mode: 'cors'}).then(res => {
            if (res.status >= 400) {
                throw new Error('Bad response from server');
            }

            return res.json();
        }).catch(error => {
            console.error(error);
        });
    }
}
