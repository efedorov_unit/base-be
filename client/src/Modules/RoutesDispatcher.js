import cookie from 'cookie';
import locale from './../Modules/locale';

function RoutesDispatcher(routes, precall, postcall) {
    this.routes = routes;
    this.precall = precall;
    this.postcall = postcall;
    this.lastHash = '';

    this.redirectTo = ( route, captures ) => {
        const hash = this.urlFor(route, captures);
        this.redirectToHash( hash );
    };

    this.redirectToHash = ( hash ) => {
        if ( window.location.hash === hash) {
            this.reload(hash);
        } else {
            window.location.hash = hash;
        }
    };

    this.start = () => {
        $(window).unbind('hashchange').bind('hashchange', (e) => {
            if ( window.location.hash.replace(/;.+$/, '') === this.lastHash.replace(/;.+$/, '') ) {
                return;
            }
            this.lastHash = window.location.hash;
            this._dispatch(e);
        }).trigger('hashchange');
    };

    this.reload = (hash) => {
        return this._dispatch({
            originalEvent: {
                oldURL: hash || window.location.hash,
                newURL: hash || window.location.hash
            }
        });
    };

    this.absoluteUrlFor = (routeName, captures) => {
        return window.location.href.split(/\?|#/)[0] + this.urlFor(routeName, captures);
    };

    this.urlFor = (routeName, captures) => {
        // Find pattern
        let pattern;
        for (let i = 0; i < this.routes.length; i++ ) {
            if ( routeName === this.routes[i].name ) {
                pattern = this.routes[i].pattern;
                break;
            }
        }

        if (!pattern) return routeName;

        const keys = Object.keys(captures || {});
        keys.forEach(key => {
            const re = new RegExp(`[:*]${  key}`, 'g');
            pattern = pattern.replace(re, captures[key]);
        });

        // Clean not replaces placeholders
        pattern = pattern.replace(/[:*][^/.]+/g, '');

        return `#${  pattern}`;
    };

    this.match = (locationHash) => {
        let location = locationHash.replace(/^#/, '');
        location = location.replace(/;.+$/, '');

        const captures = {};
        let route;
        this.routes.forEach((innerRoute) => {
            const pattern = innerRoute.pattern.replace(/:\w+/g, '([^/]+)');
            const re = new RegExp(`^${  pattern  }$`);
            const matched = location.match(re);
            if ( matched ) {
                // Find capture names
                const capturesNames = [];
                const capturesRe = /:(\w+)/g;
                let found;
                while ((found = capturesRe.exec(innerRoute.pattern)) !== null) {
                    capturesNames.push(found[1]);

                }

                // Collect found captures
                capturesNames.forEach((name, index) => {
                    captures[name] = matched[index + 1];
                });

                // Take route
                route = innerRoute;
            }
        });

        let res = {};
        if (route) {
            captures.route = route;
            res = captures;
        } else {
            if ( window.location.hash && window.location.hash !== '#' ) {
                this.redirectToHash('');
            }
            res = null;
        }

        return res;
    };

    this._dispatch = async () => {
        const hash = window.location.href.replace(/^.+#/, '#');
        const localeData = await this.initLocale();
        locale.init({ localeData, locale: window.lang });

        const captures = this.match(hash) || this.match(window.location.pathname);

        if ( captures ) {
            const route = captures.route;

            if ( $.isFunction( this.precall ) ) {
                if (!this.precall(captures)) return;
            }

            if ( $.isFunction( route.cb ) ) {
                this.routeObj = route.cb(captures);
            } else {
                this.routeObj = false;
            }

            if ( $.isFunction( this.postcall ) ) {
                this.postcall(captures);
            }
        }
    };

    this.initLocale = () => {
        const cookieLang = cookie.parse(document.cookie).locale;

        window.lang = cookieLang && cookieLang !== 'null' ? cookieLang : 'ru';

        return fetch(`/admin/static/locales/admin/${  window.lang  }.json`, {mode: 'cors'}).then(res => {
            if (res.status >= 400) {
                throw new Error('Bad response from server');
            }

            return res.json();
        }).catch(error => {
            console.error(error);
        });
    };
}

export default RoutesDispatcher;
