import Promise from 'bluebird';
import 'whatwg-fetch';

export function get(url, opts = {credentials: 'include'}) {
    return fetch(url, opts).then(res => {
        if (!res.ok) return Promise.reject(new Error(`${res.url}, ${res.status}`));

        return res.json().then(json => ({ json, res }));
    }).then(({ json, res }) => {
        redirectIfError(json);

        if (!res.ok || json.Error) {
            return Promise.reject(new Error(JSON.stringify(json)));
        }

        return json;
    }).catch(err => Promise.reject(err));
}

$.postJSON = function (url, data, callback) {
    return $.ajax({
        type : 'POST',
        url,
        data,
        dataType : 'json',
        success : (res) => {
            redirectIfError(res);
            if (callback !== undefined) {
                return callback(res);
            }
        }
    });
};

$.postMultiPart = function (url, data, callback) {
    return $.ajax({
        'type' : 'POST',
        url,
        data,
        'dataType' : 'json',
        'success' : res => {
            redirectIfError(res);
            if (callback !== undefined) {
                return callback(res);
            }
        },
        processData: false,
        contentType: false
    });
};

$.deleteJSON = function (url, data, callback) {
    return $.ajax({
        'type' : 'DELETE',
        url,
        data,
        'dataType' : 'json',
        'success' : res => {
            redirectIfError(res);
            if (callback !== undefined) {
                return callback(res);
            }
        }
    });
};

export function loadTemplate(selector, html) {
    return new Promise((res) => {
        $(selector).load(html, () => {
            res();
        });
    });
}

export function paramsObj(obj = {}) {
    return new Proxy(obj, {
        set(target, prop, value) {
            if (typeof prop !== 'undefined' && prop && typeof value !== 'undefined' && value !== '')
                target[prop] = value;

            return true;
        }
    });
}

// HARDCODED REDIRECT
function redirectIfError(res) {
    const isWithHash = window.location.hash;

    if (res.Error) {
        if (res.Error.Type === 'ACCESS_DENIED' && isWithHash) {
            window.location.href = '/admin';
        }
    }
}
