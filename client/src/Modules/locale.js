import Jed from 'jed';

class Locale {
    init({ localeData, locale }) {
        this.jed = new Jed(localeData);
        this.locale = locale;
    }

    gettext = (text) => {
        return this.jed.gettext(text);
    };

    ngettext = (singular, plural, amount) => {
        return this.jed.ngettext(singular, plural, amount);
    };

    sprintf = (text, ...params) => {
        return this.jed.sprintf(text, ...params);
    }

    getLocale = () => {
        return this.locale.toLowerCase();
    };
}

// HARDCODE
const locale = new Locale();

export default locale;
