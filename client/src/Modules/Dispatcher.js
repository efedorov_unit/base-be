import ROUTES from './Routes.js';

export default class Dispatcher {
    // HISTORY = createBrowserHistory();

    static state = [];

    redirect(url = '/') {
        if (this.state.slice(-1).pop() !== url) {
            this.state.push(url);
            this.HISTORY.push(url);
        }
    }

    start() {
        $('a.route').on('click', ev => {
            ev.preventDefault();
            this.redirect($(ev.target).attr('href'));
        });

        this.HISTORY.listen(location => {
            ROUTES.forEach(route => {
                const matched = this.matched(route, location.pathname);

                if (matched) {
                    let found;
                    const captures = {};
                    const capturesNames = [];
                    const capturesRe = /:(\w+)/g;
                    while ((found = capturesRe.exec(route.pattern)) !== null) {
                        capturesNames.push(found[1]);
                    }
                    capturesNames.forEach((name, index) => {
                        captures[name] = matched[index + 1];
                    });
                    route.cb(captures);
                }
            });

        });
    }

    matched(route, path) {
        const pattern = route.pattern.replace(/:\w+/g, '([^/]+)');
        const re = new RegExp(`${pattern  }$`);

        return `/${path}`.match(re);
    }
}
