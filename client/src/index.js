import 'babel-polyfill';

import $ from 'jquery';
import 'admBootstrap';
import 'bootstrap';
import 'bootstrapCss';
import 'admCss';
import 'moment';
import 'admSkin';
import 'admin-lte';

import Main from './Main';

$(document).ready(() => {
    const main = new Main();
    main.init();
});
